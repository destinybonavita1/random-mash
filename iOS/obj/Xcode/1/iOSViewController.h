// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface iOSViewController : UIViewController {
	UIButton *_loginBtn;
	UITextField *_passwordText;
	UIButton *_registerBtn;
	UITextField *_usernameText;
}

@property (nonatomic, retain) IBOutlet UIButton *loginBtn;

@property (nonatomic, retain) IBOutlet UITextField *passwordText;

@property (nonatomic, retain) IBOutlet UIButton *registerBtn;

@property (nonatomic, retain) IBOutlet UITextField *usernameText;

@end
