﻿using System;
using System.Collections.Generic;
using System.Linq;

using GoogleAnalytics.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Core;
using System.Linq.Expressions;
using System.Threading;
using System.Drawing;
using System.Net;
using System.IO;
using System.Diagnostics;
using MonoTouch.CoreText;
using MonoTouch.AudioToolbox;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Globalization;
using System.Text.RegularExpressions;

namespace iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		public static Security security;
		public static SuperViewController vc;
		public static UIStoryboard storyboard;
		public static SidebarTableViewController infoVc;
		public static string iOSVersion = UIDevice.CurrentDevice.SystemVersion;
		public static UITabBarController tabBarController = new UITabBarController ();

		public static IGAITracker Tracker;
		public static readonly string TrackingId = "UA-54095167-2";

		public override UIWindow Window {
			get;
			set;
		}

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			if (double.Parse (Regex.Matches (iOSVersion, "^[0-9]+") [0].Value) >= 8.0) {
				this.RegisterActions (new object[] {
					new object[] {
						new object[] { "Dislike", true },
						new object[] { "Like", true }
					}, 
					new object[] {
						new object[] { "Profile", true, UIUserNotificationActivationMode.Foreground },
						new object[] { "Follow", true }
					}, 
					new object[] {
						new object[] { "Profile", true, UIUserNotificationActivationMode.Foreground },
						new object[] { "Unfollow", true }
					}
				}, new string[] { 
					"posts", "user.notfollowing", "user.following"
				});

				app.RegisterForRemoteNotifications ();
			} else
				app.RegisterForRemoteNotificationTypes (UIRemoteNotificationType.None | UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound);

			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
			 
			GAI.SharedInstance.DispatchInterval = 5;
			GAI.SharedInstance.TrackUncaughtExceptions = true;

			Tracker = GAI.SharedInstance.GetTracker (TrackingId);
			security = new Security ();

			switch (UIDevice.CurrentDevice.UserInterfaceIdiom) {
			case UIUserInterfaceIdiom.Phone:
				{
					storyboard = UIStoryboard.FromName ("MainStoryboard", null);

					break;
				}
			}

			if (options != null) {
				if (options.ContainsKey (UIApplication.LaunchOptionsRemoteNotificationKey))
					this.Views ((options [UIApplication.LaunchOptionsRemoteNotificationKey] as NSDictionary));
				else
					this.Views ();
			} else
				this.Views ();

			Reachability.RemoteHostStatus ();
			Reachability.ReachabilityChanged += delegate(object sender, EventArgs e) {
				if (Reachability.RemoteHostStatus () == NetworkStatus.NotReachable) {
					UIViewController v = new UIViewController ();
					v.View.BackgroundColor = UIColor.White;
					v.NavigationItem.Title = "Random Mash";

					UINavigationController reach = new UINavigationController (v);
					app.Windows [0].RootViewController = reach;
				} else {
					if (Info.Categories.Length == 0)
						Info.GetCategories ();

					this.Views ();
				}
			};

			this.NSNotifications ();

			NSData token = NSUserDefaults.StandardUserDefaults.ValueForKey (new NSString ("device.user.token")) as NSData;
			if (token != null)
				this.RegisteredForRemoteNotifications (UIApplication.SharedApplication, token);

			return true;
		}

		public void NSNotifications ()
		{
			UIApplication app = UIApplication.SharedApplication;
			NSNotificationCenter.DefaultCenter.AddObserver ("LoginStatusChanged", delegate {
				this.Views ();

				Tracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "user", "status changed.", 0).Build ());
			});
		}

		public void RegisterActions (object[] titles, string[] categories)
		{
			List<UIMutableUserNotificationCategory> categoriesList = new List<UIMutableUserNotificationCategory> ();
			for (int i = 0; i < categories.Length; i++) {
				List<UIMutableUserNotificationAction> actions = new List<UIMutableUserNotificationAction> ();
				foreach (object[] title in (titles[i] as object[])) {
					UIMutableUserNotificationAction action = new UIMutableUserNotificationAction ();
					action.Identifier = string.Format ("com.destinedmodel.{0}.{1}", categories [i], title [0]);
					action.Title = title [0].ToString ();
					action.AuthenticationRequired = Convert.ToBoolean (title [1]);

					if (title.Length > 2)
						action.ActivationMode = (UIUserNotificationActivationMode)title [2];
					else
						action.ActivationMode = UIUserNotificationActivationMode.Background;
					actions.Add (action);
				}

				UIMutableUserNotificationCategory category = new UIMutableUserNotificationCategory ();
				category.Identifier = string.Format ("com.destinedmodel.{0}", categories [i]);
				category.SetActions (actions.ToArray<UIMutableUserNotificationAction> (), UIUserNotificationActionContext.Default);
				categoriesList.Add (category);
			}

			NSSet categoriesSet = new NSSet (categoriesList.ToArray<UIMutableUserNotificationCategory> ());
			UIApplication.SharedApplication.RegisterUserNotificationSettings (UIUserNotificationSettings.GetSettingsForTypes (UIUserNotificationType.Alert | UIUserNotificationType.Badge, categoriesSet));
		}

		public void Views (NSDictionary notification = null)
		{
			UIApplication app = UIApplication.SharedApplication;
			if (security.IsLoggedIn ()) {
				vc = (SuperViewController)storyboard.InstantiateViewController ("MainView");
				vc.LoadView ();

				SetTabViewControllers (notification);

				SidebarTableViewController options = new SidebarTableViewController ();
				UINavigationController nav3 = new UINavigationController (options);
				options.NavigationItem.Title = "Options";

				infoVc = new SidebarTableViewController (false);
				UINavigationController nav4 = new UINavigationController (infoVc);
				infoVc.NavigationItem.Title = "Info";

				vc.AddView (nav3, new RectangleF (0, 0, (float)(app.Windows [0].Frame.Width), UIScreen.MainScreen.Bounds.Height));
				vc.AddView (tabBarController, new RectangleF ((float)(app.Windows [0].Frame.Width), 0, app.Windows [0].Frame.Width, UIScreen.MainScreen.Bounds.Height));
				vc.AddView (nav4, new RectangleF ((float)((app.Windows [0].Frame.Width) + app.Windows [0].Frame.Width), 0, (float)(app.Windows [0].Frame.Width), UIScreen.MainScreen.Bounds.Height));

				vc.InfoViewVisibility ();
				app.Windows [0].RootViewController = vc;
			} else {
				UIViewController LoginView = (UIViewController)storyboard.InstantiateViewController ("LoginView");
				UINavigationController nav = (UINavigationController)storyboard.InstantiateInitialViewController ();
				LoginView.NavigationItem.Title = "Random Mash";

				UIViewController[] vcs = new UIViewController[1] { LoginView };

				nav.SetViewControllers (vcs, false);
				app.Windows [0].RootViewController = nav;
			}
		}

		public void SetTabViewControllers (NSDictionary notification = null)
		{
			HomeTableViewController home = new HomeTableViewController ();
			UINavigationController homeNav = new UINavigationController (home);
			home.NavigationItem.Title = "Home";
			home.SetSource ("Jokes Feed");

			HomeTableViewController profile = new HomeTableViewController ();
			UINavigationController profileNav = new UINavigationController (profile);
			profile.NavigationItem.Title = AppDelegate.security.Username;
			profile.SetSource ("Profile", AppDelegate.security.UserId);

			SearchTableViewController search = new SearchTableViewController ();
			UINavigationController searchNav = new UINavigationController (search);
			search.NavigationItem.Title = "Search";

			PostViewController post = (PostViewController)storyboard.InstantiateViewController ("PostView");
			UINavigationController postNav = new UINavigationController (post);
			post.SetupNavigiationBar (ComposerType.Normal);

			NotificationTableViewController notif = new NotificationTableViewController ();
			UINavigationController notifNav = new UINavigationController (notif);
			notif.NavigationItem.Title = "Notification";

			if (notification != null) {
				if (notification.ContainsKey (NSObject.FromObject ("data"))) {
					if ((notification ["data"] as NSDictionary).ContainsKey (NSObject.FromObject ("PostId"))) {
						LoadNotificationView (notification);
					}
				}
			}

			tabBarController.TabBar.Hidden = true;
			tabBarController.ViewControllers = new UIViewController[] {
				homeNav,
				profileNav,
				searchNav,
				postNav,
				notifNav
			};
		}

		public async void LoadNotificationView (NSDictionary notification)
		{
			JObject data = (await (new Posts ()).GetPost (security.UserId, (notification ["data"] as NSDictionary) ["PostId"].ToString ())) [0];
			PostViewerViewControllerController postView = new PostViewerViewControllerController (data, new Dictionary<int, UIImage> ());
			postView.NavigationItem.Title = "Post";

			InvokeOnMainThread (() => {
				(tabBarController.ViewControllers [0] as UINavigationController).PushViewController (postView, true);
			});
		}

		public static void PushViewController (object view)
		{
			UINavigationController nav = (UINavigationController)tabBarController.ViewControllers [tabBarController.SelectedIndex];
		
			nav.PushViewController (view as UIViewController, true);
		}

		public static void PresentViewController (object view, NSAction handler = null)
		{
			UIApplication.SharedApplication.Windows [0].RootViewController.PresentViewController (view as UIViewController, true, handler);
		}

		public static void SendGlobalMessage (string message, bool popup = false, int postId = 0)
		{
			UIView view = new UIView (new RectangleF (0, -200, UIApplication.SharedApplication.Windows [0].Frame.Width, 64));
			view.BackgroundColor = UIColor.White;

			UITextView text = new UITextView (new RectangleF (0, 20, UIApplication.SharedApplication.Windows [0].Frame.Width, 44));
			text.Text = message;
			text.TextAlignment = UITextAlignment.Center;
			text.Editable = false;
			text.Selectable = false;
			text.Font = UIFont.SystemFontOfSize (15.0f);
			view.AddSubview (text);

			(UIApplication.SharedApplication.Windows [0].RootViewController as UIViewController).View.AddSubview (view);

			UIView.Animate (2, 0, UIViewAnimationOptions.TransitionNone | UIViewAnimationOptions.AllowUserInteraction, () => {
				RectangleF frame = new RectangleF (0, -100, UIApplication.SharedApplication.Windows [0].Frame.Width, 64);
				frame.Y = 0;

				view.Frame = frame;
			}, null);

			UIApplication.SharedApplication.Windows [0].RootViewController.View.UserInteractionEnabled = true;
			view.UserInteractionEnabled = true;
			view.AddGestureRecognizer (new UITapGestureRecognizer ((NSAction)delegate {
				if (popup && postId != 0) {
					if (security.IsLoggedIn ()) {
						UIAlertView alert = new UIAlertView ("Random Mash", message, null, "Close", new string[] { "View" });
						alert.Clicked += async delegate(object sender, UIButtonEventArgs e) {
							string title = (sender as UIAlertView).ButtonTitle (e.ButtonIndex);
							if (title == "View") {
								JObject data = (await (new Posts ()).GetPost (security.UserId, postId.ToString ())) [0];
								PostViewerViewControllerController postView = new PostViewerViewControllerController (data, new Dictionary<int, UIImage> ());
								postView.NavigationItem.Title = "Post";

								PushViewController (postView);
							}
						};

						alert.Show ();
					} else {
						UIAlertView alert = new UIAlertView ("Random Mash", message, null, "Close", null);
						alert.Show ();
					}
				}

				view.RemoveFromSuperview ();
			}));

			SystemSound.Vibrate.PlayAlertSound ();
		}

		public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
		{
			User user = new User ();
			NSUserDefaults.StandardUserDefaults.SetValueForKey (deviceToken, new NSString ("device.user.token"));
			NSUserDefaults.StandardUserDefaults.Synchronize ();

			bool isDebuging = false;
			#if DEBUG
			isDebuging = true;
			#endif

			string token = deviceToken.Description.Replace ("<", "").Replace (">", "").Replace (" ", "");
			if (security.IsLoggedIn ()) {
				user.RegisterNotifications ("iOS", token, isDebuging, security.UserId);
			} else {
				user.RegisterNotifications ("iOS", token, isDebuging);
			}

			Tracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "user", "registered remote notification", 0).Build ());
		}

		public override void FailedToRegisterForRemoteNotifications (UIApplication application, NSError error)
		{
			Console.Write (error.Description);

			Tracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "user", "failed to register", 0).Build ());
		}

		public override void HandleAction (UIApplication application, string actionIdentifier, NSDictionary remoteNotificationInfo, Action completionHandler)
		{
			security = new Security ();
			if (Reachability.RemoteHostStatus () != NetworkStatus.NotReachable && security.IsLoggedIn ()) {
				if (actionIdentifier.ToLower () == "com.destinedmodel.posts.like") {
					(new Posts ()).LikePost (security.UserId, (remoteNotificationInfo ["data"] as NSDictionary) ["PostId"].ToString ());
				}
				if (actionIdentifier.ToLower () == "com.destinedmodel.posts.dislike") {
					(new Posts ()).DislikePost (security.UserId, (remoteNotificationInfo ["data"] as NSDictionary) ["PostId"].ToString ());
				}
			}
		}

		public override void DidReceiveRemoteNotification (UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			NSDictionary aps = (userInfo.ValueForKey (new NSString ("aps")) as NSDictionary);
			NSDictionary data = (userInfo.ValueForKey (new NSString ("data")) as NSDictionary);

			if (data.ValueForKey (new NSString ("Post")) != null && data.ValueForKey (new NSString ("PostId")) != null)
				SendGlobalMessage (data.ValueForKey (new NSString ("Post")).ToString (), true, Convert.ToInt32 (data.ValueForKey (new NSString ("PostId")).ToString ()));
			else {
				if (aps.ValueForKey (new NSString ("alert")) != null)
					SendGlobalMessage (aps.ValueForKey (new NSString ("alert")).ToString (), true);
			}

//			if (aps.ValueForKey (new NSString ("alert")) != null) {
//				SendGlobalMessage (aps.ValueForKey (new NSString ("alert") as NSString).ToString ());
//			}

			Tracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "user", "received remote notification", 0).Build ());
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}

		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{

		}
		
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}
		
		// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{

		}
		
		// This method is called when the application is about to terminate. Save data, if needed.
		public override void WillTerminate (UIApplication application)
		{

		}
	}
}

