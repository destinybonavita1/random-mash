﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Newtonsoft.Json.Linq;
using Core;
using System.Threading.Tasks;
using System.Collections.Generic;
using MonoTouch.ObjCRuntime;

namespace iOS
{
	public class PostViewerViewControllerSource : UITableViewSource
	{
		public JObject data;
		public UITableView tableView;
		public List<JObject> comments = new List<JObject> ();
		public Dictionary<int, UIImage> imgs = new Dictionary<int, UIImage> ();

		public PostViewerViewControllerSource (UITableView tableView, JObject data, Dictionary<int, UIImage> imgs)
		{
			this.data = data;
			this.imgs = imgs;
			tableView.ReloadData ();
			this.tableView = tableView;

			GetPostComments ();
		}

		public async void GetPostComments ()
		{
			comments = await (new Posts ()).GetPostComments (data ["PostId"].ToString ());

			InvokeOnMainThread (() => {
				this.tableView.ReloadData ();
			});
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			int section = indexPath.Section;
			switch (section) {
			case 0:
				{
					bool hasImage = Convert.ToBoolean (data ["HasImage"].ToObject<int> ());
					var cell = new UITableViewCell ();
					if (hasImage) {
						cell = tableView.DequeueReusableCell (HomeTableViewImageCell.Key) as HomeTableViewControllerCell;
						if (cell == null)
							cell = new HomeTableViewImageCell (this.LikePost (indexPath), this.DislikePost (indexPath));
					} else {
						cell = tableView.DequeueReusableCell (HomeTableViewControllerCell.Key) as HomeTableViewControllerCell;
						if (cell == null)
							cell = new HomeTableViewControllerCell (this.LikePost (indexPath), this.DislikePost (indexPath));
					}

					if (hasImage) {
						(cell as HomeTableViewImageCell).postId = data ["PostId"].ToObject<int> ();
						(cell as HomeTableViewImageCell).newLabel.Lines = 999999999;
						(cell as HomeTableViewImageCell).newLabel.Text = data ["Post"].ToString ();
						(cell as HomeTableViewImageCell).newLabel.TextAlignment = UITextAlignment.Center;
						(cell as HomeTableViewImageCell).SelectionStyle = UITableViewCellSelectionStyle.None;
					} else {
						(cell as HomeTableViewControllerCell).postId = data ["PostId"].ToObject<int> ();
						(cell as HomeTableViewControllerCell).newLabel.Lines = 999999999;
						(cell as HomeTableViewControllerCell).newLabel.Text = data ["Post"].ToString ();
						(cell as HomeTableViewControllerCell).newLabel.TextAlignment = UITextAlignment.Center;
						(cell as HomeTableViewControllerCell).SelectionStyle = UITableViewCellSelectionStyle.None;
					}

					if (hasImage) {
						object i = data ["Image"].ToObject<object> ();
						if (i.GetType () == "".GetType () && !imgs.ContainsKey (data ["PostId"].ToObject<int> ())) {
							Task.Run (() => {
								NSUrl url = NSUrl.FromString (i.ToString () + "&w=350");
								NSData image = NSData.FromUrl (url);
								UIImage img = UIImage.LoadFromData (image);

								InvokeOnMainThread (() => {
									(cell as HomeTableViewImageCell).imageDisplay.Image = img;

									data ["Image"] = 1;

									if (!imgs.ContainsKey (data ["PostId"].ToObject<int> ()))
										imgs.Add (data ["PostId"].ToObject<int> (), img);
								});
							});
						} else {
							(cell as HomeTableViewImageCell).imageDisplay.Image = imgs [data ["PostId"].ToObject<int> ()];
						}

						HomeTableViewImageCell c = (cell as HomeTableViewImageCell);
						string liked = (data ["Liked"].ToObject<bool> ()) ? "Liked" : "Likes";
						c.likeButton.Title = String.Format ("({0}) " + liked, data ["Likes"].ToObject<int> ());

						string disliked = (data ["Disliked"].ToObject<bool> ()) ? "Disliked" : "Dislikes";
						c.dislikeButton.Title = String.Format ("({0}) " + disliked, data ["Dislikes"].ToObject<int> ());
					} else {
						HomeTableViewControllerCell c = (cell as HomeTableViewControllerCell);
						string liked = (data ["Liked"].ToObject<bool> ()) ? "Liked" : "Likes";
						c.likeButton.Title = String.Format ("({0}) " + liked, data ["Likes"].ToObject<int> ());

						string disliked = (data ["Disliked"].ToObject<bool> ()) ? "Disliked" : "Dislikes";
						c.dislikeButton.Title = String.Format ("({0}) " + disliked, data ["Dislikes"].ToObject<int> ());
					}

					return cell;
				}
			case 1:
				{
					var cell = tableView.DequeueReusableCell (PostViewerViewControllerCell.Key) as PostViewerViewControllerCell;
					if (cell == null)
						cell = new PostViewerViewControllerCell ();
					cell.TextLabel.Text = comments [indexPath.Row] ["Comment"].ToString ();
					cell.DetailTextLabel.Text = comments [indexPath.Row] ["Alias"].ToString ();

					string UID = AppDelegate.security.UserId;
					if (data ["UserId"].ToString () == UID || comments [indexPath.Row] ["UID"].ToString () == UID) {
						UILongPressGestureRecognizer rec = new UILongPressGestureRecognizer ();
						rec.AddTarget (this, new Selector ("handleLongPress:"));

						cell.AddGestureRecognizer (rec);
//						cell.AddGestureRecognizer (new UILongPressGestureRecognizer (delegate(UILongPressGestureRecognizer obj) {
//							if (obj.State == UIGestureRecognizerState.Began) {
//								UIActionSheet sheet = new UIActionSheet ("Random Mash", null, "Cancel", "Delete", null);
//								sheet.Clicked += delegate(object sender, UIButtonEventArgs e) {
//									string button = (sender as UIActionSheet).ButtonTitle (e.ButtonIndex);
//									if (button == "Delete") {
//										Posts posts = new Posts ();
//										posts.DeleteComment (AppDelegate.security.UserId, data ["PostId"].ToString (), comments [indexPath.Row] ["CommentID"].ToString (), delegate() {
//											InvokeOnMainThread (() => {
//												tableView.BeginUpdates ();
//
//												comments.RemoveAt (indexPath.Row);
//												tableView.DeleteRows (new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Automatic);
//
//												tableView.EndUpdates ();
//											});
//										});
//									}
//								};
//
//								sheet.ShowInView (this.tableView);
//							}
//						}));
					}

					return cell;
				}
			default: 
				{
					return null;
				}
			}
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return 2;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			switch (section) {
			case 0:
				{
					return 1;
				}
			case 1:
				{
					return comments.Count;
				}
			default:
				{
					return 0;
				}
			}
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if (indexPath.Section == 0) {
				bool isImage = Convert.ToBoolean (data ["HasImage"].ToObject<int> ());
				string txt = data ["Post"].ToString ();

				UILabel lbl = new UILabel (new RectangleF (0, ((isImage == true) ? 210 : 0), tableView.Frame.Size.Width - 50, 20));
				lbl.Text = txt;
				lbl.Lines = 999999999;
				lbl.TextAlignment = UITextAlignment.Center;

				lbl.SizeToFit ();
				return lbl.Frame.Size.Height + 35 + ((isImage == true) ? (44 + 210) : 44);
			} else {
				return 44;
			}
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (indexPath.Section == 1) {
				HomeTableViewController profile = new HomeTableViewController ();
				profile.NavigationItem.Title = comments [indexPath.Row] ["Alias"].ToString ();
				profile.SetSource ("Profile", comments [indexPath.Row] ["UID"].ToString ());

				AppDelegate.PushViewController (profile);
			}

			tableView.DeselectRow (indexPath, true);
		}

		public EventHandler LikePost (NSIndexPath indexPath)
		{
			return delegate {
				bool likedb = data ["Liked"].ToObject<bool> ();
				int likes = data ["Likes"].ToObject<int> ();
				if (likedb) {
					data ["Likes"] = likes - 1;
				} else {
					data ["Likes"] = likes + 1;
				}

				data ["Liked"] = !likedb;
				NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewReload", null);
				new Posts ().LikePost (AppDelegate.security.UserId.ToString (), data ["PostId"].ToString ());
				this.tableView.ReloadData ();
			};
		}

		public EventHandler DislikePost (NSIndexPath indexPath)
		{
			return delegate {
				bool likedb = data ["Disliked"].ToObject<bool> ();
				int likes = data ["Dislikes"].ToObject<int> ();
				if (likedb) {
					data ["Dislikes"] = likes - 1;
				} else {
					data ["Dislikes"] = likes + 1;
				}

				data ["Disliked"] = !likedb;
				NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewReload", null);
				new Posts ().DislikePost (AppDelegate.security.UserId.ToString (), data ["PostId"].ToString ());
				this.tableView.ReloadData ();
			};
		}

		[Export ("handleLongPress:")]
		public void handleLongPress (UILongPressGestureRecognizer recon)
		{
			if (recon.State == UIGestureRecognizerState.Began) {
				UIActionSheet sheet = new UIActionSheet ("Random Mash", null, "Cancel", "Delete", null);
				sheet.Clicked += delegate(object sender, UIButtonEventArgs e) {
					string button = (sender as UIActionSheet).ButtonTitle (e.ButtonIndex);
					if (button == "Delete") {
						Posts posts = new Posts ();
						NSIndexPath indexPath = tableView.IndexPathForCell ((recon.View as PostViewerViewControllerCell));
						posts.DeleteComment (AppDelegate.security.UserId, data ["PostId"].ToString (), comments [indexPath.Row] ["CommentID"].ToString (), delegate() {
							InvokeOnMainThread (() => {
								tableView.BeginUpdates ();

								comments.RemoveAt (indexPath.Row);
								tableView.DeleteRows (new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Automatic);

								tableView.EndUpdates ();
							});
						});
					}
				};

				sheet.ShowInView (tableView);
			}
		}
	}
}

