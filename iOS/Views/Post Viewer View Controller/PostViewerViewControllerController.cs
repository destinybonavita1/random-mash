﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core;

namespace iOS
{
	public class PostViewerViewControllerController : UITableViewController
	{
		public JObject data;
		public Dictionary<int, UIImage> imgs = new Dictionary<int, UIImage> ();

		public PostViewerViewControllerController (JObject data, Dictionary<int, UIImage> imgs) : base (UITableViewStyle.Grouped)
		{
			this.data = data;
			this.imgs = imgs;

			UIRefreshControl refresher = new UIRefreshControl ();
			refresher.ValueChanged += delegate {
				NSNotificationCenter.DefaultCenter.PostNotificationName ("ReloadComments", null);
			};
			TableView.AddSubview (refresher);

			NSNotificationCenter.DefaultCenter.AddObserver ("ReloadComments", delegate {
				if (refresher.Refreshing)
					refresher.EndRefreshing ();

				(TableView.Source as PostViewerViewControllerSource).GetPostComments ();
			});
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			UIBarButtonItem button = new UIBarButtonItem (UIBarButtonSystemItem.Compose);
			button.Clicked += delegate(object sender, EventArgs e) {
				PostViewController post = (PostViewController)AppDelegate.storyboard.InstantiateViewController ("PostView");
				UINavigationController postNav = new UINavigationController (post);
				post.SetupNavigiationBar (ComposerType.Comment);
				post.PostId = this.data ["PostId"].ToString ();

				AppDelegate.PresentViewController (postNav, null);
			};

			this.NavigationItem.RightBarButtonItem = button;
			TableView.Source = new PostViewerViewControllerSource (this.TableView, this.data, this.imgs);
		}

		public override void ViewDidAppear (bool animated)
		{
			SidebarTableViewController info = AppDelegate.infoVc;
			info.source.Sections = new object[] { "Poster", "Category", "Liked", "Disliked" };
			info.source.Options = new object[] {
				new Dictionary<string, string> () { 
					{ "text", data ["Alias"].ToString () }, 
					{ "UserId", data ["UserId"].ToString () }
				}, data ["Category"].ToString (), "Loading liked...", "Loading disliked..."
			};

			info.TableView.ReloadData ();
			AppDelegate.vc.InfoViewVisibility ();

			new Action (async delegate() {
				Posts posts = new Posts ();
				JObject[] likes = await posts.GetLikes (AppDelegate.security.UserId, data ["PostId"].ToString ());
				JObject[] dislikes = await posts.GetDislikes (AppDelegate.security.UserId, data ["PostId"].ToString ());

				info.source.Options = new object[] {
					new Dictionary<string, string> () { 
						{ "text", data ["Alias"].ToString () }, 
						{ "UserId", data ["UserId"].ToString () },
						{ "Following", data ["Following"].ToString () }
					}, data ["Category"].ToString (), likes, dislikes
				};

				InvokeOnMainThread (() => {
					info.TableView.ReloadData ();

					//I needed to test it, okay?
					//AppDelegate.SendGlobalMessage ("There are so many things to say, I just don't know where to start!");
				});
			}).Invoke ();

			base.ViewDidAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			AppDelegate.vc.InfoViewVisibility ();

			base.ViewDidDisappear (animated);
		}
	}
}

