﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS
{
	public class PostViewerViewControllerCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("PostViewerViewControllerCell");

		public PostViewerViewControllerCell () : base (UITableViewCellStyle.Subtitle, Key)
		{

		}
	}
}

