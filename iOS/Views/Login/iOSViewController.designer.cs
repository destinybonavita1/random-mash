// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

namespace iOS
{
	[Register ("iOSViewController")]
	partial class iOSViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton loginBtn { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField passwordText { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton registerBtn { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField usernameText { get; set; }

		[Action ("login:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void login (UIButton sender);

		[Action ("register:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void register (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
