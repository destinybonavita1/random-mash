﻿using System;
using System.Drawing;

using Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using GoogleAnalytics.iOS;

namespace iOS
{
	public partial class iOSViewController : UIViewController
	{
		public iOSViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			SetDelegates ();

			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Login View");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateAppView ().Build ());
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}

		#endregion

		#region Actions

		partial void login (UIButton sender)
		{
			loginCall ();
		}

		partial void register (UIButton sender)
		{
			registerCall ();
		}

		#endregion

		#region Calls

		protected async void loginCall ()
		{
			if (usernameText.Text.Replace (" ", "").Length > 0 && passwordText.Text.Replace (" ", "").Length > 0) {
				User user = new User ();
				JObject task = await user.Login (usernameText.Text, passwordText.Text);
				Console.WriteLine (task ["error"]);

				if (task ["error"] != null) {
					new UIAlertView ("Random Mash", task ["error"].ToString (), null, "Okay", null).Show ();
				} else {
					AppDelegate.security.Login (int.Parse (task ["UID"].ToString ()), usernameText.Text, passwordText.Text);
					NSNotificationCenter.DefaultCenter.PostNotificationName ("LoginStatusChanged", null);
				}
			} else {
				new UIAlertView ("Random Mash", "Please type a username and password.", null, "Okay", null).Show ();
			}
		}

		protected async void registerCall ()
		{
			if (usernameText.Text.Replace (" ", "").Length > 0 && passwordText.Text.Replace (" ", "").Length > 0) {
				User user = new User ();
				JObject task = await user.Register (usernameText.Text, passwordText.Text);
				Console.WriteLine (task ["error"]);

				if (task ["error"] != null) {
					new UIAlertView ("Random Mash", task ["error"].ToString (), null, "Okay", null).Show ();
				} else {
					AppDelegate.security.Login (int.Parse (task ["UID"].ToString ()), usernameText.Text, passwordText.Text);
					NSNotificationCenter.DefaultCenter.PostNotificationName ("LoginStatusChanged", null);
				}
			} else {
				new UIAlertView ("Random Mash", "Please type a username and password.", null, "Okay", null).Show ();
			}
		}

		#endregion

		#region Delegates

		protected void SetDelegates ()
		{
			this.usernameText.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				return false;
			};

			this.passwordText.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				return false;
			};
		}

		#endregion
	}
}