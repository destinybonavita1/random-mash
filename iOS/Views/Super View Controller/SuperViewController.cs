﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Diagnostics;
using Core;
using GoogleAnalytics.iOS;

namespace iOS
{
	public partial class SuperViewController : UIViewController
	{
		private bool infoVisible = true;

		public SuperViewController (IntPtr handle) : base (handle)
		{
		}

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear (bool animated)
		{
			this.View.BackgroundColor = UIColor.White;
			this.pageViewController.BackgroundColor = UIColor.Blue;
			this.pageViewController.KeyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag;
			this.pageViewController.Bounces = false;
			this.pageViewController.PagingEnabled = true;

			base.ViewWillAppear (animated);

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Main View");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateAppView ().Build ());
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}

		#endregion

		#region ScrollView Subviews

		public void AddView (object viewController, RectangleF rect)
		{
			this.AddChildViewController (viewController as UIViewController);

			UIView controller = (viewController as UIViewController).View;
			controller.Frame = rect;	
			controller.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;

			this.pageViewController.AddSubview (controller);

			SizeF contentSize = this.pageViewController.ContentSize;
			contentSize.Width += rect.Width;

			this.pageViewController.ContentSize = contentSize;
			this.pageViewController.SetContentOffset (new PointF ((float)(UIApplication.SharedApplication.Windows [0].Frame.Width), 0), true);
		}

		public void InfoViewVisibility ()
		{
			if (infoVisible) {
				SizeF contentSize = this.pageViewController.ContentSize;
				contentSize.Height = (float)(UIApplication.SharedApplication.Windows [0].Frame.Height);
				contentSize.Width -= (float)(UIApplication.SharedApplication.Windows [0].Frame.Width);

				this.pageViewController.ContentSize = contentSize;
			} else {
				SizeF contentSize = this.pageViewController.ContentSize;
				contentSize.Height = (float)(UIApplication.SharedApplication.Windows [0].Frame.Height);
				contentSize.Width += (float)(UIApplication.SharedApplication.Windows [0].Frame.Width);

				this.pageViewController.ContentSize = contentSize;
			}

			this.infoVisible = !this.infoVisible;
		}

		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			SizeF contentSize = SizeF.Empty;
			foreach (UIView view in this.pageViewController.Subviews) {
				contentSize.Width += view.Frame.Width;
			}

			this.pageViewController.ScrollRectToVisible (new RectangleF (contentSize.Width, 0, 0, 0), true);
			this.pageViewController.ContentSize = contentSize;
		}

		#endregion
	}
}

