﻿
using System;
using System.Drawing;

using System.Net.Http;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Core;
using System.Diagnostics;
using System.IO;
using System.Net;
using MBProgressHUD;

namespace iOS
{
	public class CategoriesTableViewSource : UITableViewSource
	{
		private string txt;
		private bool isPost;
		private UIImage image;
		private string selectedCat = "";

		public CategoriesTableViewSource (bool isPost = false, UIImage image = null, string txt = null)
		{
			this.isPost = isPost;
			this.image = image;
			this.txt = txt;

			NSNotificationCenter.DefaultCenter.AddObserver ("SendPost", delegate {
				if (selectedCat != "") {
					this.Post ();
				}
			});
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return (isPost) ? Info.Categories.Length - 1 : Info.Categories.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (CategoriesTableViewCell.Key) as CategoriesTableViewCell;
			if (cell == null)
				cell = new CategoriesTableViewCell ();
			cell.TextLabel.Text = (isPost) ? Info.Categories [indexPath.Row + 1] : Info.Categories [indexPath.Row];

			if (isPost && cell.TextLabel.Text == selectedCat)
				cell.Accessory = UITableViewCellAccessory.Checkmark;
			else
				cell.Accessory = UITableViewCellAccessory.None;
			
			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (!this.isPost) {
				AppDelegate.security.selectedCategory = Info.Categories [indexPath.Row];
				NSNotificationCenter.DefaultCenter.PostNotification (NSNotification.FromName ("CategoryChangedNotification", NSString.FromObject (Info.Categories [indexPath.Row])));
			} else {
				selectedCat = Info.Categories [indexPath.Row + 1];
				tableView.ReloadData ();
			}
		}

		private bool post = false;

		public void Post ()
		{
			if (!post) {
				string baseImage = null;
				if (this.image != null) {
					baseImage = this.image.AsJPEG (0.5f).GetBase64EncodedString (NSDataBase64EncodingOptions.None);
				}

				var hud = new MTMBProgressHUD (UIApplication.SharedApplication.Windows [0]) {
					LabelText = ((baseImage != null) ? "Uploading your photo..." : "Sending your post..."),
					RemoveFromSuperViewOnHide = true,
					TaskInProgress = false
				};

				UIApplication.SharedApplication.Windows [0].AddSubview (hud);
				hud.Show (true);

				(new Posts ()).Post (AppDelegate.security.UserId, selectedCat, txt, WebUtility.UrlEncode (baseImage), delegate() {
					hud.Hide (true, 0.5f);

					AppDelegate.tabBarController.SelectedIndex = 0;
					(AppDelegate.tabBarController.ViewControllers [3] as UINavigationController).PopToRootViewController (false);

					AppDelegate.security.selectedFilter = "Recent";
					NSNotificationCenter.DefaultCenter.PostNotification (NSNotification.FromName ("FilterChangedNotification", NSString.FromObject ("Recent")));
				});

			}

			post = true;
		}
	}
}

