﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS
{
	public class CategoriesTableViewCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("CategoriesTableViewCell");

		public CategoriesTableViewCell () : base (UITableViewCellStyle.Value1, Key)
		{

		}
	}
}

