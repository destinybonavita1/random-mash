﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Core;
using System.Threading.Tasks;

namespace iOS
{
	public class CategoriesTableViewController : UITableViewController
	{
		private string txt = null;
		private bool isPost = false;
		private UIImage image = null;
		private UIRefreshControl refresh;

		public CategoriesTableViewController (bool isPost = false, UIImage image = null, string txt = null) : base (UITableViewStyle.Grouped)
		{
			this.txt = txt;
			this.image = image;
			this.isPost = isPost;
			refresh = new UIRefreshControl ();
			refresh.ValueChanged += async delegate {
				await Info.GetCategories ();

				InvokeOnMainThread (() => {
					this.TableView.ReloadData ();
					this.refresh.EndRefreshing ();
				});
			};

			this.TableView.AddSubview (refresh);
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Register the TableView's data source
			TableView.Source = new CategoriesTableViewSource (this.isPost, image, txt);
		}
	}
}

