﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace iOS
{
	public class Sidebar_Table_ViewSource : UITableViewSource
	{
		private bool isOptions;
		public object[] Options = new object[] {
			new object[] { "Profile", "Search" },
			new object[] { "New Post", "Jokes Feed", "Notifications" },
			"Logout"
		};
		public object[] Sections = new object[] { "Users", "Posts", "Settings" };

		public Sidebar_Table_ViewSource (bool isOptions = true)
		{
			this.isOptions = isOptions;
		}

		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return Options.Length;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			object obj = Options [section];
			if (obj.GetType () == "".GetType ())
				return 1;
			else if (obj.GetType () == (new object[] { }).GetType ())
				return (obj as object[]).Length;
			else if (obj.GetType () == (new Dictionary<string, string> ()).GetType ())
				return 1;
			else if (obj.GetType () == (new JObject[] { }).GetType ())
				return (obj as JObject[]).Length;
			else
				return 1;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (Sidebar_Table_ViewCell.Key) as Sidebar_Table_ViewCell;
			if (cell == null)
				cell = new Sidebar_Table_ViewCell ();

			if (isOptions) {
				object obj = Options [indexPath.Section];
				string title = "";
				if (obj.GetType () == "".GetType ()) {
					title = obj.ToString ();
				} else {
					title = (obj as object[]) [indexPath.Row].ToString ();
				}

				cell.TextLabel.Text = title;
				if (title == "Search") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("search-100.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				} else if (title == "Profile") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("user-128.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				} else if (title == "Logout") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("exit-128.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				} else if (title == "New Post") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("edit_property-100.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				} else if (title == "Jokes Feed") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("lol-100.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				} else if (title == "Messages") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("message-100.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				} else if (title == "Notifications") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("note-128.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				}
			} else {
				object obj = Options [indexPath.Section];
				string title = "";
				if (obj.GetType () == "".GetType ()) {
					title = obj.ToString ();
				} else if (obj.GetType () == (new object[] { }).GetType ()) {
					title = (obj as object[]) [indexPath.Row].ToString ();
				} else if (obj.GetType () == (new Dictionary<string, string> ()).GetType ()) {
					title = (obj as Dictionary<string, string>) ["text"];
				} else if (obj.GetType () == (new JObject[] { }).GetType ()) {
					title = (obj as JObject[]) [indexPath.Row] ["Alias"].ToString ();
				}

				cell.TextLabel.Text = title;

				string sectionTitle = this.Sections [indexPath.Section].ToString ();
				if (sectionTitle == "Poster" || sectionTitle == "Liked" || sectionTitle == "Disliked") {
					UIImageView imgView = new UIImageView (UIImage.FromFile ("user-128.png"));
					imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
					imgView.Frame = new RectangleF (0, 0, 30, 30);

					cell.AccessoryView = imgView;
				} 
			}

			return cell;
		}

		EventHandler ended;

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			AppDelegate.vc.pageViewController.SetContentOffset (new PointF ((float)(UIApplication.SharedApplication.Windows [0].Frame.Width), 0), true);

			if (this.isOptions) {
				string selected = "";
				object obj = Options [indexPath.Section];
				if (obj.GetType () == "".GetType ())
					selected = obj.ToString ();
				else
					selected = (obj as object[]) [indexPath.Row].ToString ();

				if (selected == "Profile") {
					AppDelegate.tabBarController.SelectedIndex = 1;
				} else if (selected == "New Post") {
					AppDelegate.tabBarController.SelectedIndex = 3;
				} else if (selected == "Jokes Feed") {
					AppDelegate.tabBarController.SelectedIndex = 0;
				}/* else if (selected == "Messages") {

				}*/ else if (selected == "Notifications") {
					AppDelegate.tabBarController.SelectedIndex = 4;
				} else if (selected == "Logout") {
					AppDelegate.security.Logout ();
					NSNotificationCenter.DefaultCenter.PostNotificationName ("LoginStatusChanged", null);
				} else if (selected == "Search") {
					AppDelegate.tabBarController.SelectedIndex = 2;
				}
			} else {
				ended = delegate(object sender, EventArgs e) {
					object obj = Options [indexPath.Section];
					if (obj.GetType () == "".GetType ()) {
						//					string title = this.Sections [indexPath.Section].ToString ();
						//					title = null;

						//Handle string later
					} else if (obj.GetType () == (new object[] { }).GetType ()) {
						//Handle array
					} else if (obj.GetType () == (new Dictionary<string, string> ()).GetType ()) {
						Dictionary<string, string> dict = (obj as Dictionary<string, string>);
						HomeTableViewController profile = new HomeTableViewController ();
						profile.NavigationItem.Title = dict ["text"].ToString ();
						profile.SetSource ("Profile", dict ["UserId"].ToString (), Convert.ToBoolean (dict ["Following"]));

						AppDelegate.PushViewController (profile);
					} else if (obj.GetType () == (new JObject[] { }).GetType ()) {
						JObject dict = (obj as JObject[]) [indexPath.Row];
						HomeTableViewController profile = new HomeTableViewController ();
						profile.NavigationItem.Title = dict ["Alias"].ToString ();
						profile.SetSource ("Profile", dict ["UserId"].ToString (), dict ["Following"].ToObject<bool> ());

						AppDelegate.PushViewController (profile);
					}

					AppDelegate.vc.pageViewController.ScrollAnimationEnded -= ended;
				};

				AppDelegate.vc.pageViewController.ScrollAnimationEnded += ended;
			}

			tableView.DeselectRow (indexPath, true);
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return this.Sections [section].ToString ();
		}
	}
}

