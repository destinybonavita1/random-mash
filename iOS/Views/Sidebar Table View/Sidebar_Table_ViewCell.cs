﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS
{
	public class Sidebar_Table_ViewCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("Sidebar_Table_ViewCell");

		public Sidebar_Table_ViewCell () : base (UITableViewCellStyle.Default, Key)
		{

		}
	}
}

