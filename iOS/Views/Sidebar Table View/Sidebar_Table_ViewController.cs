﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.iAd;

namespace iOS
{
	public class SidebarTableViewController : UITableViewController
	{
		private bool isOptions;
		public Sidebar_Table_ViewSource source;

		public SidebarTableViewController (bool isOptions = true) : base (UITableViewStyle.Grouped)
		{
			this.isOptions = isOptions;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Register the TableView's data source
			source = new Sidebar_Table_ViewSource (isOptions);
			TableView.Source = source;

//Add when old account expires
//			TableView.TableFooterView = new ADBannerView (ADAdType.Banner);
//
//			(TableView.TableFooterView as ADBannerView).Hidden = true;
//			(TableView.TableFooterView as ADBannerView).AdLoaded += delegate(object sender, EventArgs e) {
//				(sender as ADBannerView).Hidden = false;
//			};
//			(TableView.TableFooterView as ADBannerView).FailedToReceiveAd += delegate(object sender, AdErrorEventArgs e) {
//				(sender as ADBannerView).RemoveFromSuperview ();
//			};
		}
	}
}

