﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using Core;
using System.Security.Cryptography.X509Certificates;
using GoogleAnalytics.iOS;

namespace iOS
{
	[Register ("HomeTableViewController")]
	public class HomeTableViewController : UITableViewController
	{
		private string UserId;
		private UIRefreshControl refresh;
		private HomeTableViewControllerSource source;

		public HomeTableViewController () : base (UITableViewStyle.Grouped)
		{
			refresh = new UIRefreshControl ();
			refresh.ValueChanged += delegate {
				source = (HomeTableViewControllerSource)TableView.Source;
				if (!source.isProfile)
					source.GetPosts ();
				else
					source.GetUserPosts (source.UserId.ToString ());
			};

			TableView.AddSubview (refresh);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public override void ViewDidAppear (bool animated)
		{
			this.TableView.ReloadData ();

			base.ViewDidAppear (animated);
		}

		public void SetSource (string viewType, string UserId = "0", bool following = false)
		{
			this.UserId = UserId;

			this.AddBarButtons (viewType, following);
			this.addNotifications (viewType);
			TableView.Source = new HomeTableViewControllerSource (viewType, Convert.ToInt32 (UserId));
			source = (HomeTableViewControllerSource)TableView.Source;
		}

		public void addNotifications (string viewType)
		{
			#region Etc...

			NSNotificationCenter.DefaultCenter.AddObserver ("HomeTableViewReload", delegate {
				InvokeOnMainThread (() => {
					refresh.EndRefreshing ();
					this.TableView.ReloadData ();
				});
			});

			NSNotificationCenter.DefaultCenter.AddObserver ("HomeTableViewDeleteCell", delegate(NSNotification obj) {
				InvokeOnMainThread (() => {
					TableView.BeginUpdates ();
					NSIndexPath path = NSIndexPath.FromItemSection (0, 0);
					if (obj.Object.GetType () == typeof(HomeTableViewControllerCell))
						path = (obj.Object as HomeTableViewControllerCell).indexPath;
					else if (obj.Object.GetType () == typeof(HomeTableViewImageCell))
						path = (obj.Object as HomeTableViewImageCell).indexPath;

					source.posts.RemoveAt (path.Row);
					TableView.DeleteRows (new NSIndexPath[] { path }, UITableViewRowAnimation.Automatic);

					TableView.EndUpdates ();
				});
			});

			#endregion

			#region Settings

			if (viewType == "Jokes Feed") {
				NSNotificationCenter.DefaultCenter.AddObserver ("CategoryChangedNotification", delegate {
					((HomeTableViewControllerSource)this.TableView.Source).GetPosts ();

					this.TableView.ReloadData ();
					this.NavigationItem.RightBarButtonItem.Title = AppDelegate.security.selectedCategory;
					this.close ();
				});

				NSNotificationCenter.DefaultCenter.AddObserver ("FilterChangedNotification", delegate {
					((HomeTableViewControllerSource)this.TableView.Source).GetPosts ();

					this.TableView.ReloadData ();
					this.NavigationItem.LeftBarButtonItem.Title = AppDelegate.security.selectedFilter;
					this.close ();
				});
			}

			#endregion
		}

		public void AddBarButtons (string view, bool following = false)
		{
			if (view == "Jokes Feed") {
				UIBarButtonItem postsType = new UIBarButtonItem (AppDelegate.security.selectedFilter, UIBarButtonItemStyle.Plain, delegate {
					FilterTableViewController filter = new FilterTableViewController ();
					UINavigationController nav = new UINavigationController (filter);
					filter.NavigationItem.Title = "Filter";

					UIBarButtonItem closeButton = new UIBarButtonItem ("Close", UIBarButtonItemStyle.Plain, delegate {
						this.close ();
					});

					filter.NavigationItem.RightBarButtonItem = closeButton;
					this.PresentViewController (nav, true, null);
				});

				UIBarButtonItem categoryBtn = new UIBarButtonItem (AppDelegate.security.selectedCategory, UIBarButtonItemStyle.Plain, delegate {
					CategoriesTableViewController cats = new CategoriesTableViewController ();
					UINavigationController nav = new UINavigationController (cats);
					cats.NavigationItem.Title = "Categories";

					UIBarButtonItem closeButton = new UIBarButtonItem ("Close", UIBarButtonItemStyle.Plain, delegate {
						this.close ();
					});

					cats.NavigationItem.RightBarButtonItem = closeButton;
					this.PresentViewController (nav, true, null);
				});

				this.NavigationItem.LeftBarButtonItem = postsType;
				this.NavigationItem.RightBarButtonItem = categoryBtn;
			} else if (view == "Other Profile") {

			} else if (view == "Profile") {
				if (UserId != AppDelegate.security.UserId) {
					new NSAction (async delegate() {
						following = await (new User ().IsFollowing (AppDelegate.security.UserId, UserId));

						UIBarButtonItem followBtn = new UIBarButtonItem ((following) ? "Unfollow" : "Follow", UIBarButtonItemStyle.Plain, delegate(object sender, EventArgs e) {
							User u = new User ();
							u.Follow (AppDelegate.security.UserId, this.UserId);

							UIBarButtonItem obj = (UIBarButtonItem)sender;
							if (obj.Title == "Follow") {
								obj.Title = "Unfollow";
							} else if (obj.Title == "Unfollow") {
								obj.Title = "Follow";
							}

							GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "user", "follow hit", 0).Build ());
						});

						this.NavigationItem.RightBarButtonItem = followBtn;
					}).Invoke ();
				} else {
					this.NavigationItem.RightBarButtonItem = null;
				}

				this.NavigationItem.LeftBarButtonItem = null;
			}
		}

		public void close ()
		{
			this.DismissViewController (true, null);
		}
	}
}

