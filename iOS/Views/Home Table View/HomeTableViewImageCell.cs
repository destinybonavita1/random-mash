﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using MonoTouch.MessageUI;
using Core;

namespace iOS
{
	public partial class HomeTableViewImageCell : UITableViewCell
	{
		public int postId;
		public bool isOwner;
		public UILabel newLabel;
		public UIToolbar statsBar;
		public NSIndexPath indexPath;
		public UIImageView imageDisplay;
		public UIBarButtonItem likeButton;
		public UIBarButtonItem dislikeButton;
		public static readonly NSString Key = new NSString ("HomeTableViewImageCell");

		public HomeTableViewImageCell (EventHandler like, EventHandler dislike) : base ()
		{
			imageDisplay = new UIImageView (new RectangleF (0, 10, this.Frame.Width, 200));
			imageDisplay.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			imageDisplay.ContentMode = UIViewContentMode.ScaleAspectFit;
			this.ContentView.AddSubview (imageDisplay);

			imageDisplay.UserInteractionEnabled = true;
			imageDisplay.AddGestureRecognizer (new UITapGestureRecognizer ((NSAction)delegate {
				UIViewController v = new UIViewController ();
				UINavigationController nav = new UINavigationController (v);

				UIImageView imgView = new UIImageView (UIApplication.SharedApplication.Windows [0].Frame);
				imgView.ContentMode = UIViewContentMode.ScaleAspectFit;
				imgView.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
				imgView.Image = imageDisplay.Image;
				imgView.BackgroundColor = UIColor.White;
				imgView.UserInteractionEnabled = true;
				imgView.AddGestureRecognizer (new UILongPressGestureRecognizer (this, new Selector ("handleLongPressImage:")));

				v.View.AddSubview (imgView);
				v.NavigationItem.Title = "Image";

				v.NavigationItem.RightBarButtonItem = new UIBarButtonItem ("Close", UIBarButtonItemStyle.Plain, delegate {
					nav.DismissViewController (true, null);
				});

				nav.NavigationBar.Translucent = false;
				UIApplication.SharedApplication.Windows [0].RootViewController.PresentViewController (nav, true, null);
			}));

			newLabel = new UILabel (new RectangleF (0, 210, this.Frame.Width, this.Frame.Height - (44 + 210)));
			newLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
			newLabel.Font = UIFont.ItalicSystemFontOfSize (17);
			this.ContentView.AddSubview (newLabel);

			statsBar = new UIToolbar (new RectangleF (0, this.Frame.Size.Height - 44, this.Frame.Width, 44));
			statsBar.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth;
			statsBar.BarTintColor = UIColor.FromRGB (186, 255, 248);

			likeButton = new UIBarButtonItem ("(0) Likes", UIBarButtonItemStyle.Plain, like);
			dislikeButton = new UIBarButtonItem ("(0) Dislikes", UIBarButtonItemStyle.Plain, dislike);

			UIBarButtonItem sep = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);
			statsBar.SetItems (new UIBarButtonItem[] { likeButton, sep, dislikeButton }, false);

			this.AddGestureRecognizer (new UILongPressGestureRecognizer (this, new Selector ("handleLongPress:")));
			this.ContentView.AddSubview (statsBar); 
		}

		[Export ("handleLongPress:")]
		public void handleLongPress (UILongPressGestureRecognizer recon)
		{
			if (recon.State == UIGestureRecognizerState.Began) {
				UIActionSheet sheet = new UIActionSheet ("Random Mash", null, "Cancel", null, new string[] {
					"Twitter",
					"Facebook",
					"Message"
				});

				if (isOwner) {
					sheet.Add ("Delete");
					sheet.DestructiveButtonIndex = sheet.ButtonCount - 1;
				}

				sheet.Clicked += delegate(object sender, UIButtonEventArgs e) {
					string title = (sheet as UIActionSheet).ButtonTitle (e.ButtonIndex);
					if (title == "Twitter") {

					} else if (title == "Facebook") {

					} else if (title == "Message") {
						if (MFMessageComposeViewController.CanSendText) {
							MFMessageComposeViewController m = new MFMessageComposeViewController ();
							m.Body = '"' + this.newLabel.Text + '"' + "\n\nhttp://www.randommash.com/?i=" + postId.ToString ();
							m.Finished += delegate(object sender2, MFMessageComposeResultEventArgs e2) {
								(sender2 as MFMessageComposeViewController).DismissViewController (true, null);
							};

							UIApplication.SharedApplication.Windows [0].RootViewController.PresentViewController (m, true, null);
						}
					} else if (title == "Delete") {
						Posts posts = new Posts ();
						posts.DeletePost (AppDelegate.security.UserId, postId.ToString ());


						NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewDeleteCell", this);
					}
				};

				sheet.ShowInView (this);
			}
		}

		[Export ("handleLongPressImage:")]
		public void handleLongPressImage (UILongPressGestureRecognizer recon)
		{
			if (recon.State == UIGestureRecognizerState.Began) {
				UIActionSheet sheet = new UIActionSheet ("Random Mash", null, "Close", null, new string[] { "Save Image" });
				sheet.Clicked += delegate(object sender, UIButtonEventArgs e) {
					if ((sheet as UIActionSheet).ButtonTitle (e.ButtonIndex) == "Save Image") {
						(recon.View as UIImageView).Image.SaveToPhotosAlbum (null);
					}
				};

				sheet.ShowInView (this);
			}
		}
	}
}

