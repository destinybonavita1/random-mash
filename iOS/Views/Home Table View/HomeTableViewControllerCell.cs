﻿using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Social;
using MonoTouch.Accounts;
using MonoTouch.MessageUI;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using System.Runtime.InteropServices;
using Core;

namespace iOS
{
	public class HomeTableViewControllerCell : UITableViewCell
	{
		public int postId;
		public bool isOwner;
		public UILabel newLabel;
		public UIToolbar statsBar;
		public NSIndexPath indexPath;
		public UIBarButtonItem likeButton;
		public UIBarButtonItem dislikeButton;
		public static readonly NSString Key = new NSString ("HomeTableViewControllerCell");

		public HomeTableViewControllerCell (EventHandler like, EventHandler dislike) : base ()
		{
			RectangleF statsBarFrame = RectangleF.Empty;
			RectangleF labelFrame = RectangleF.Empty;
			statsBarFrame = new RectangleF (0, this.Frame.Size.Height - 44, this.Frame.Width, 44);
			labelFrame = new RectangleF (0, 0, this.Frame.Width, this.Frame.Height - 44);

			newLabel = new UILabel (labelFrame);
			newLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
			this.ContentView.AddSubview (newLabel);

			statsBar = new UIToolbar (statsBarFrame);
			statsBar.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth;
			statsBar.BarTintColor = UIColor.FromRGB (186, 255, 248);

			likeButton = new UIBarButtonItem ("(0) Likes", UIBarButtonItemStyle.Plain, like);
			dislikeButton = new UIBarButtonItem ("(0) Dislikes", UIBarButtonItemStyle.Plain, dislike);

			UIBarButtonItem sep = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);
			statsBar.SetItems (new UIBarButtonItem[] { likeButton, sep, dislikeButton }, false);

			this.AddGestureRecognizer (new UILongPressGestureRecognizer (this, new Selector ("handleLongPress:")));
			this.ContentView.AddSubview (statsBar); 
		}

		[Export ("handleLongPress:")]
		public void handleLongPress (UILongPressGestureRecognizer recon)
		{
			if (recon.State == UIGestureRecognizerState.Began) {
				UIActionSheet sheet = new UIActionSheet ("Random Mash", null, "Cancel", null, new string[] {
					"Twitter",
					"Facebook",
					"Message"
				});

				if (isOwner) {
					sheet.Add ("Delete");
					sheet.DestructiveButtonIndex = sheet.ButtonCount - 1;
				}

				sheet.Clicked += delegate(object sender, UIButtonEventArgs e) {
					string title = (sheet as UIActionSheet).ButtonTitle (e.ButtonIndex);
					if (title == "Twitter") {
						if (SLComposeViewController.IsAvailable (SLServiceKind.Twitter)) {
							SLComposeViewController composer = SLComposeViewController.FromService (SLServiceKind.Twitter);
							composer.SetInitialText ('"' + this.newLabel.Text + '"');
							composer.AddUrl (NSUrl.FromString ("http://www.randommash.com/?i=" + postId.ToString ()));
							composer.CompletionHandler += delegate(SLComposeViewControllerResult obj) {
								composer.DismissViewController (true, null);
							};

							UIApplication.SharedApplication.Windows [0].RootViewController.PresentViewController (composer, true, null);
						} else {
							new UIAlertView ("Random Mash", "You do not have a twitter account setup on your device.", null, "Close", null).Show ();
						}
					} else if (title == "Facebook") {
						if (SLComposeViewController.IsAvailable (SLServiceKind.Facebook)) {
							SLComposeViewController composer = SLComposeViewController.FromService (SLServiceKind.Facebook);
							composer.SetInitialText ('"' + this.newLabel.Text + '"');
							composer.AddUrl (NSUrl.FromString ("http://www.randommash.com/?i=" + postId.ToString ()));
							composer.CompletionHandler += delegate(SLComposeViewControllerResult obj) {
								composer.DismissViewController (true, null);
							};

							UIApplication.SharedApplication.Windows [0].RootViewController.PresentViewController (composer, true, null);
						} else {
							new UIAlertView ("Random Mash", "You do not have a facebook account setup on your device.", null, "Close", null).Show ();
						}
					} else if (title == "Message") {
						if (MFMessageComposeViewController.CanSendText) {
							MFMessageComposeViewController m = new MFMessageComposeViewController ();
							m.Body = '"' + this.newLabel.Text + '"' + "\n\nhttp://www.randommash.com/?i=" + postId.ToString ();
							m.Finished += delegate(object sender2, MFMessageComposeResultEventArgs e2) {
								(sender2 as MFMessageComposeViewController).DismissViewController (true, null);
							};

							UIApplication.SharedApplication.Windows [0].RootViewController.PresentViewController (m, true, null);
						}
					} else if (title == "Delete") {
						Posts posts = new Posts ();
						posts.DeletePost (AppDelegate.security.UserId, postId.ToString ());


						NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewDeleteCell", this);
					}
				};

				sheet.ShowInView (this);
			}
		}
	}
}

