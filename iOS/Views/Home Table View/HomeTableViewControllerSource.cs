﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using Core;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using MonoTouch.ObjCRuntime;
using Newtonsoft.Json;
using MonoTouch.ImageIO;
using MonoTouch.CoreImage;

namespace iOS
{
	public class HomeTableViewControllerSource : UITableViewSource
	{
		public int UserId = 0;
		public bool isProfile = false;
		public List<JObject> posts = new List<JObject> ();
		public Dictionary<int, UIImage> imgs = new Dictionary<int, UIImage> ();

		public HomeTableViewControllerSource (string Type, int UserId = 0)
		{
			if (Reachability.InternetConnectionStatus () != NetworkStatus.NotReachable) {
				if (Type == "Jokes Feed")
					GetPosts ();
				else if (Type == "Profile") {
					isProfile = true;
					this.UserId = UserId;
					GetUserPosts (UserId.ToString ());
				}
			}
		}

		public async void GetPosts ()
		{
			Posts p = new Posts ();
			posts = await p.GetPosts (AppDelegate.security.UserId, AppDelegate.security.selectedFilter, AppDelegate.security.selectedCategory);

			new CancellationTokenSource ().Cancel ();
			NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewReload", null);
		}

		public async void GetUserPosts (string userId)
		{
			Posts p = new Posts ();
			posts = await p.GetUserPosts (AppDelegate.security.UserId, userId, "All");

			new CancellationTokenSource ().Cancel ();
			NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewReload", null);
		}

		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			if (posts.Count == 0)
				return 1;
			else
				return posts.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			if (posts.Count > 0) {
				JObject post = posts [indexPath.Row];
				bool hasImage = Convert.ToBoolean (post ["HasImage"].ToObject<int> ());
				var cell = new UITableViewCell ();
				if (hasImage) {
					cell = tableView.DequeueReusableCell (HomeTableViewImageCell.Key) as HomeTableViewControllerCell;
					if (cell == null)
						cell = new HomeTableViewImageCell (this.LikePost (indexPath), this.DislikePost (indexPath));
				} else {
					cell = tableView.DequeueReusableCell (HomeTableViewControllerCell.Key) as HomeTableViewControllerCell;
					if (cell == null)
						cell = new HomeTableViewControllerCell (this.LikePost (indexPath), this.DislikePost (indexPath));
				}

				if (hasImage) {
					if (post ["UserId"].ToString () == AppDelegate.security.UserId) {
						(cell as HomeTableViewImageCell).isOwner = true;
					}

					(cell as HomeTableViewImageCell).indexPath = indexPath;
					(cell as HomeTableViewImageCell).postId = post ["PostId"].ToObject<int> ();
					(cell as HomeTableViewImageCell).newLabel.Lines = 999999999;
					(cell as HomeTableViewImageCell).newLabel.Text = post ["Post"].ToString ();
					(cell as HomeTableViewImageCell).newLabel.TextAlignment = UITextAlignment.Center;
					(cell as HomeTableViewImageCell).SelectionStyle = UITableViewCellSelectionStyle.None;
				} else {
					if (post ["UserId"].ToString () == AppDelegate.security.UserId) {
						(cell as HomeTableViewControllerCell).isOwner = true;
					}

					(cell as HomeTableViewControllerCell).indexPath = indexPath;
					(cell as HomeTableViewControllerCell).postId = post ["PostId"].ToObject<int> ();
					(cell as HomeTableViewControllerCell).newLabel.Lines = 999999999;
					(cell as HomeTableViewControllerCell).newLabel.Text = post ["Post"].ToString ();
					(cell as HomeTableViewControllerCell).newLabel.TextAlignment = UITextAlignment.Center;
					(cell as HomeTableViewControllerCell).SelectionStyle = UITableViewCellSelectionStyle.None;
				}

				if (hasImage) {
					object i = post ["Image"].ToObject<object> ();
					if (i.GetType () == "".GetType () && !imgs.ContainsKey (post ["PostId"].ToObject<int> ())) {
						Task.Run (() => {
							NSUrl url = NSUrl.FromString (i.ToString () + "&w=350");
							NSData image = NSData.FromUrl (url);
							UIImage img = UIImage.LoadFromData (image);

							InvokeOnMainThread (() => {
								(cell as HomeTableViewImageCell).imageDisplay.Image = img;

								post ["Image"] = 1;

								if (!imgs.ContainsKey (post ["PostId"].ToObject<int> ()))
									imgs.Add (post ["PostId"].ToObject<int> (), img);
								posts [indexPath.Row] = post;
							});
						});
					} else {
						(cell as HomeTableViewImageCell).imageDisplay.Image = imgs [post ["PostId"].ToObject<int> ()];
					}

					HomeTableViewImageCell c = (cell as HomeTableViewImageCell);
					string liked = (post ["Liked"].ToObject<bool> ()) ? "Liked" : "Likes";
					c.likeButton.Title = String.Format ("({0}) " + liked, post ["Likes"].ToObject<int> ());

					string disliked = (post ["Disliked"].ToObject<bool> ()) ? "Disliked" : "Dislikes";
					c.dislikeButton.Title = String.Format ("({0}) " + disliked, post ["Dislikes"].ToObject<int> ());
				} else {
					HomeTableViewControllerCell c = (cell as HomeTableViewControllerCell);
					string liked = (post ["Liked"].ToObject<bool> ()) ? "Liked" : "Likes";
					c.likeButton.Title = String.Format ("({0}) " + liked, post ["Likes"].ToObject<int> ());

					string disliked = (post ["Disliked"].ToObject<bool> ()) ? "Disliked" : "Dislikes";
					c.dislikeButton.Title = String.Format ("({0}) " + disliked, post ["Dislikes"].ToObject<int> ());
				}

				return cell;
			} else {
				var cell = new UITableViewCell (UITableViewCellStyle.Default, "EmptyTableCell");
				cell.TextLabel.Text = "There aren't any posts here.";

				return cell;
			}
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if (posts.Count > 0) {
				JObject post = posts [indexPath.Row];
				bool isImage = Convert.ToBoolean (post ["HasImage"].ToObject<int> ());
				string txt = post ["Post"].ToString ();

				UILabel lbl = new UILabel (new RectangleF (0, ((isImage == true) ? 210 : 0), tableView.Frame.Size.Width - 50, 20));
				lbl.Text = txt;
				lbl.Lines = 999999999;
				lbl.TextAlignment = UITextAlignment.Center;

				lbl.SizeToFit ();
				return lbl.Frame.Size.Height + 35 + ((isImage == true) ? (44 + 210) : 44);
			} else {
				return 44;
			}
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (posts.Count > 0) {
				GetPost (posts [indexPath.Row]);
			}

			tableView.DeselectRow (indexPath, true);
		}

		public override UITableViewCellEditingStyle EditingStyleForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return UITableViewCellEditingStyle.Delete;
		}

		public override void CommitEditingStyle (UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		{
			if (editingStyle == UITableViewCellEditingStyle.Delete) {
				JObject post = posts [indexPath.Row];
				if (post ["UserId"].ToString () == AppDelegate.security.UserId) {
					tableView.DeselectRow (indexPath, true);
				}
			}
		}

		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			if (posts.Count > 0) {
				JObject post = posts [indexPath.Row];
				if (post ["UserId"].ToString () == AppDelegate.security.UserId) {
					return true;
				}
			}

			return false;
		}

		public void GetPost (JObject data)
		{
			PostViewerViewControllerController postView = new PostViewerViewControllerController (data, imgs);
			postView.NavigationItem.Title = "Post";

			AppDelegate.PushViewController (postView);
		}

		public EventHandler LikePost (NSIndexPath indexPath)
		{
			return delegate {
				JObject post = posts [indexPath.Row];
				bool likedb = post ["Liked"].ToObject<bool> ();
				int likes = post ["Likes"].ToObject<int> ();
				if (likedb) {
					post ["Likes"] = likes - 1;
				} else {
					post ["Likes"] = likes + 1;
				}

				post ["Liked"] = !likedb;
				posts [indexPath.Row] = post;
				NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewReload", null);
				new Posts ().LikePost (AppDelegate.security.UserId.ToString (), post ["PostId"].ToString ());
			};
		}

		public EventHandler DislikePost (NSIndexPath indexPath)
		{
			return delegate {
				JObject post = posts [indexPath.Row];
				bool dislikedb = post ["Disliked"].ToObject<bool> ();
				int dislikes = post ["Dislikes"].ToObject<int> ();
				if (dislikedb) {
					post ["Dislikes"] = dislikes - 1;
				} else {
					post ["Dislikes"] = dislikes + 1;
				}

				post ["Disliked"] = !dislikedb;
				posts [indexPath.Row] = post;
				NSNotificationCenter.DefaultCenter.PostNotificationName ("HomeTableViewReload", null);
				new Posts ().DislikePost (AppDelegate.security.UserId.ToString (), post ["PostId"].ToString ());
			};
		}
	}
}

