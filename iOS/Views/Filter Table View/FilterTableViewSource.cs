﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS
{
	public class FilterTableViewSource : UITableViewSource
	{
		public string[] filters = { "Top", "Recent", "Following", "Worst" };

		public FilterTableViewSource ()
		{
		}

		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			// TODO: return the actual number of items in the section
			return filters.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (FilterTableViewCell.Key) as FilterTableViewCell;
			if (cell == null)
				cell = new FilterTableViewCell ();
			cell.TextLabel.Text = filters [indexPath.Row];
			if (cell.TextLabel.Text == "Worst")
				cell.DetailTextLabel.Text = "Why would you click this one?";
			
			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			AppDelegate.security.selectedFilter = filters [indexPath.Row];
			NSNotificationCenter.DefaultCenter.PostNotification (NSNotification.FromName ("FilterChangedNotification", NSString.FromObject (filters [indexPath.Row])));
		}
	}
}

