﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS
{
	public class NotificationTableViewCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("NotificationTableViewCell");

		public NotificationTableViewCell () : base (UITableViewCellStyle.Value1, Key)
		{
			// TODO: add subviews to the ContentView, set various colors, etc.
			TextLabel.Text = "TextLabel";
		}
	}
}

