﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Newtonsoft.Json.Linq;
using Core;
using System.Collections.Generic;

namespace iOS
{
	public class NotificationTableViewSource : UITableViewSource
	{
		UITableView tableView;
		public JObject[] notifications = new JObject[] { };

		public NotificationTableViewSource (UITableView tableView)
		{
			this.tableView = tableView;
			this.GetNotifications ();

			UIRefreshControl refresh = new UIRefreshControl ();
			refresh.ValueChanged += delegate {
				this.GetNotifications ();

				InvokeOnMainThread (() => {
					refresh.EndRefreshing ();
				});
			};

			tableView.AddSubview (refresh);
		}

		public void GetNotifications ()
		{
			new NSAction (async delegate() {
				User user = new User ();
				notifications = await user.GetNotifications (AppDelegate.security.UserId);

				InvokeOnMainThread (() => {
					tableView.ReloadData ();
				});
			}).Invoke ();
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return (notifications.Length > 0) ? notifications.Length : 1;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (NotificationTableViewCell.Key) as NotificationTableViewCell;
			if (cell == null)
				cell = new NotificationTableViewCell ();

			if (notifications.Length > 0) {
				JObject obj = notifications [indexPath.Row] ["Message"].ToObject<JObject> ();
				cell.TextLabel.Text = obj ["Message"].ToString ();
				cell.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				cell.TextLabel.Lines = 9999999;

				if (!Convert.ToBoolean (notifications [indexPath.Row].ToObject<JObject> () ["Read"].ToObject<int> ())) {
					cell.TextLabel.Font = UIFont.BoldSystemFontOfSize (17.0f);
				}
			} else {
				cell.TextLabel.Text = "This is kinda sad...you don't have any notifications.";
				cell.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				cell.TextLabel.Lines = 9999999;
			}

			return cell;
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if (notifications.Length > 0)
				return 40;
			else
				return 65;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (notifications.Length > 0) {
				new NSAction (async delegate() {
					JObject obj = notifications [indexPath.Row] ["Message"].ToObject<JObject> ();
					List<JObject> datas = await (new Posts ()).GetPost (AppDelegate.security.UserId, obj ["PostId"].ToString ());
					if (datas.Count == 0)
						return;
					PostViewerViewControllerController postView = new PostViewerViewControllerController (datas [0], new Dictionary<int, UIImage> ());
					postView.NavigationItem.Title = "Post";

					AppDelegate.PushViewController (postView);
				}).Invoke ();

				new NSAction (delegate() {
					(new User ()).ReadNotification (notifications [indexPath.Row] ["NotificationID"].ToString (), AppDelegate.security.UserId);
					GetNotifications ();
				}).Invoke ();
			}

			tableView.DeselectRow (indexPath, true);
		}
	}
}

