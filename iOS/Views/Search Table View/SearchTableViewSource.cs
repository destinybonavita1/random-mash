﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Newtonsoft.Json.Linq;
using Core;
using GoogleAnalytics.iOS;

namespace iOS
{
	public class SearchTableViewSource : UITableViewSource
	{
		public UITableView tableView;
		public JObject[] searchResults = new JObject[] { };

		public SearchTableViewSource (UITableView tableView)
		{
			this.tableView = tableView;
		}

		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			if (searchResults.Length == 0)
				return 1;
			else
				return searchResults.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (SearchTableViewCell.Key) as SearchTableViewCell;
			if (cell == null)
				cell = new SearchTableViewCell ();
			if (searchResults.Length == 0)
				cell.TextLabel.Text = "There are no search results.";
			else {
				JObject obj = searchResults [indexPath.Row];

				cell.TextLabel.Text = obj ["Alias"].ToString ();
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			
			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (searchResults.Length > 0) {
				JObject obj = searchResults [indexPath.Row];
				HomeTableViewController profile = new HomeTableViewController ();
				profile.NavigationItem.Title = obj ["Alias"].ToString ();
				profile.SetSource ("Profile", obj ["UserId"].ToString (), (obj ["Following"].ToObject<int> () > 0) ? true : false);

				AppDelegate.PushViewController (profile);
			}

			tableView.DeselectRow (indexPath, true);
		}

		public async void DownloadSearch (string Search)
		{
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "search user", Search, 0).Build ());
			JObject[] results = await (new User ()).Search (Search, AppDelegate.security.UserId);
			this.searchResults = results;

			InvokeOnMainThread (() => {
				tableView.ReloadData ();
			});
		}
	}

	public class SearchTableViewBarDelegate : UISearchBarDelegate
	{
		public SearchTableViewSource source;

		public SearchTableViewBarDelegate (SearchTableViewSource source)
		{
			this.source = source;
		}

		public override void SearchButtonClicked (UISearchBar searchBar)
		{
			source.DownloadSearch (searchBar.Text);
		}

		public override void TextChanged (UISearchBar searchBar, string searchText)
		{
			bool settings = NSUserDefaults.StandardUserDefaults.BoolForKey ("enabled_autofill");
			if (!settings)
				settings = Reachability.InternetConnectionStatus () == NetworkStatus.ReachableViaWiFiNetwork;

			if (settings && searchText.Length > 0) {
				source.DownloadSearch (searchText);
			} else if (searchText.Length == 0) {
				source.searchResults = new JObject[] { };
				source.tableView.ReloadData ();
			}
		}
	}
}

