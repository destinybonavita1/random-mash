﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.AudioToolbox;

namespace iOS
{
	public class SearchTableViewController : UITableViewController
	{
		private UISearchBar searchBar;
		private SearchTableViewSource searchSouce;

		public SearchTableViewController () : base (UITableViewStyle.Grouped)
		{

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Register the TableView's data source
			searchSouce = new SearchTableViewSource (this.TableView);
			TableView.Source = searchSouce;

			searchBar = new UISearchBar (new RectangleF (0, 0, UIApplication.SharedApplication.Windows [0].Frame.Width, 64));
			searchBar.Delegate = new SearchTableViewBarDelegate (searchSouce);

			this.TableView.KeyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag;
			this.TableView.TableHeaderView = searchBar;
		}
	}
}

