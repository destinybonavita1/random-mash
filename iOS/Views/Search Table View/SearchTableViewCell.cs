﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS
{
	public class SearchTableViewCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("SearchTableViewCell");

		public SearchTableViewCell () : base (UITableViewCellStyle.Default, Key)
		{

		}
	}
}

