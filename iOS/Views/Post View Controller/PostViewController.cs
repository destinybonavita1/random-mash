﻿using System;
using System.Drawing;

using Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using GoogleAnalytics.iOS;
using MBProgressHUD;

namespace iOS
{
	public partial class PostViewController : UIViewController
	{
		public string PostId;
		private UIImage image;
		private float startingHeight;

		public PostViewController (IntPtr handle) : base (handle)
		{

		}

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.postText.BackgroundColor = UIColor.Clear;
			this.imageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			this.postText.KeyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag;
		}

		public override void ViewDidLayoutSubviews ()
		{
			startingHeight = this.postText.Frame.Height;

			NSNotificationCenter.DefaultCenter.AddObserver (new NSString ("UIKeyboardDidShowNotification"), delegate(NSNotification obj) {
				RectangleF keyboardFrame = (obj.UserInfo.ValueForKeyPath (new NSString ("UIKeyboardFrameEndUserInfoKey")) as NSValue).RectangleFValue;
				RectangleF newFrame = this.postText.Frame;
				newFrame.Height = (startingHeight - keyboardFrame.Height + 12);
				if (newFrame.Height < 70)
					return;

				this.postText.Frame = newFrame;
				this.imageView.Frame = newFrame;
			});

			NSNotificationCenter.DefaultCenter.AddObserver (new NSString ("UIKeyboardDidChangeFrameNotification"), delegate(NSNotification obj) {
				RectangleF keyboardFrame = (obj.UserInfo.ValueForKeyPath (new NSString ("UIKeyboardFrameEndUserInfoKey")) as NSValue).RectangleFValue;
				RectangleF newFrame = this.postText.Frame;
				newFrame.Height = (startingHeight - keyboardFrame.Height + 12);
				if (newFrame.Height < 70)
					return;

				this.postText.Frame = newFrame;
				this.imageView.Frame = newFrame;
			});

			NSNotificationCenter.DefaultCenter.AddObserver (new NSString ("UIKeyboardDidHideNotification"), delegate(NSNotification obj) {
				RectangleF newFrame = this.postText.Frame;
				newFrame.Height = startingHeight;

				this.postText.Frame = newFrame;
				this.imageView.Frame = newFrame;
			});

			this.postText.BecomeFirstResponder ();
		}

		#endregion

		public void SetupNavigiationBar (ComposerType type = ComposerType.Normal)
		{
			switch (type) {
			case ComposerType.Normal:
				{
					this.NavigationItem.Title = "Post";

					UIBarButtonItem image = new UIBarButtonItem (UIBarButtonSystemItem.Camera, delegate {
						UIActionSheet sheet = new UIActionSheet ("Random Mash", null, "Cancel", null, new string[] {
							"Camera",
							"Camera Roll"
						});

						sheet.Clicked += delegate(object sender, UIButtonEventArgs e) {
							string title = sheet.ButtonTitle (e.ButtonIndex);
							UIImagePickerController picker = new UIImagePickerController ();
							picker.Canceled += delegate(object sender2, EventArgs e2) {
								(sender2 as UIImagePickerController).DismissViewController (true, null);
							};
							picker.FinishedPickingMedia += delegate(object sender2, UIImagePickerMediaPickedEventArgs e2) {
								UIImagePickerController pick = (sender2 as UIImagePickerController);
								this.image = e2.OriginalImage;
								this.imageView.Image = this.image;

								pick.DismissViewController (true, null);
							};

							if (title == "Cancel")
								return;
							else if (title == "Camera") {
								picker.AllowsEditing = false;
								picker.SourceType = UIImagePickerControllerSourceType.Camera;
								picker.CameraDevice = UIImagePickerControllerCameraDevice.Rear;
								picker.CameraCaptureMode = UIImagePickerControllerCameraCaptureMode.Photo;
							} else if (title == "Camera Roll") {
								picker.AllowsEditing = false;
								picker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
							}

							AppDelegate.PresentViewController (picker, null);
						};

						sheet.ShowInView (this.View);
					});

					UIBarButtonItem category = new UIBarButtonItem ("Post", UIBarButtonItemStyle.Plain, delegate {
						if (this.postText.Text.Replace (" ", "").Length > 0 || this.image != null) {
							CategoriesTableViewController cats = new CategoriesTableViewController (true, this.image, this.postText.Text);
							cats.NavigationItem.Title = "Post";

							UIBarButtonItem postButton = new UIBarButtonItem ("Post", UIBarButtonItemStyle.Plain, delegate {
								this.image = null;
								this.postText.Text = "";
								this.imageView.Image = null;

								GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "user", "posted", 0).Build ());
								NSNotificationCenter.DefaultCenter.PostNotificationName ("SendPost", null);
							});

							cats.NavigationItem.RightBarButtonItem = postButton;
							AppDelegate.PushViewController (cats);
						}
					});

					this.NavigationItem.LeftBarButtonItem = image;
					this.NavigationItem.RightBarButtonItem = category;

					break;
				}
			case ComposerType.Message:
				{
					break;
				}
			case ComposerType.Comment:
				{
					this.NavigationItem.Title = "Comment";

					UIBarButtonItem closeBtn = new UIBarButtonItem ("Close", UIBarButtonItemStyle.Plain, delegate {
						this.NavigationController.DismissViewControllerAsync (true);
					});

					UIBarButtonItem writeBtn = new UIBarButtonItem ("Write", UIBarButtonItemStyle.Plain, delegate {
						if (this.postText.Text.Replace (" ", "").Length > 0 || this.image != null) {
							this.NavigationController.DismissViewController (true, new NSAction (delegate() {
								Posts posts = new Posts ();
								posts.Comment (AppDelegate.security.UserId, PostId, this.postText.Text, new Action (delegate {
									NSNotificationCenter.DefaultCenter.PostNotificationName ("ReloadComments", null);
								}));
							}));
						}
					});

					this.NavigationItem.LeftBarButtonItem = closeBtn;
					this.NavigationItem.RightBarButtonItem = writeBtn;

					break;
				}
			}
		}
	}
}

