﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

using MonoTouch.Security;
using MonoTouch.MobileCoreServices;
using Core;
using System.Threading.Tasks;
using GoogleAnalytics.iOS;

namespace iOS
{
	public class Security
	{
		public string UserId;
		public string Username;

		public Security ()
		{
			if (Reachability.InternetConnectionStatus () != NetworkStatus.NotReachable)
				Task.WaitAll (new Task[] { Info.GetCategories () }, 1000);

			selectedCategory = NSUserDefaults.StandardUserDefaults.StringForKey ("Category");
			if (selectedCategory == null)
				selectedCategory = "All";
		}

		public bool IsLoggedIn ()
		{
			SecStatusCode status;
			SecRecord rec = new SecRecord (SecKind.InternetPassword) { Server = "randommash" };

			rec = SecKeyChain.QueryAsRecord (rec, out status);
			if (status == SecStatusCode.Success) {
				UserId = rec.Creator.ToString ();
				Username = rec.Account;

				return true;
			} else
				return false;
		}

		public void Login (int UID, string Username, string password)
		{
			SecRecord rec = new SecRecord (SecKind.InternetPassword) {
				Creator = UID,
				Account = Username,
				Comment = password,
				Server = "randommash"
			};

			SecKeyChain.Add (rec);
		}

		public void Logout ()
		{
			SecKeyChain.Remove (new SecRecord (SecKind.InternetPassword) { Server = "randommash" });
		}

		public string selectedFilter { 
			get {
				string value = NSUserDefaults.StandardUserDefaults.StringForKey ("Filter");
				if (value == null)
					return "Recent";
				else
					return value;
			} 
			set {
				NSUserDefaults.StandardUserDefaults.SetString (value.ToString (), "Filter");
				NSUserDefaults.StandardUserDefaults.Synchronize ();
			} 
		}

		public string selectedCategory { 
			get {
				string value = NSUserDefaults.StandardUserDefaults.StringForKey ("Category");
				if (value == null)
					return "All";
				else
					return value;
			} 
			set {
				if (value == null)
					value = "All";

				GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateEvent ("app", "category", value.ToString (), 0).Build ());
				NSUserDefaults.StandardUserDefaults.SetString (value.ToString (), "Category");
				NSUserDefaults.StandardUserDefaults.Synchronize ();
			} 
		}
	}
}

