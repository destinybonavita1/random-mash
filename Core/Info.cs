﻿using System;
using System.Net;
using System.IO;
using System.Text;
using System.Linq;

using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading;

namespace Core
{
	public class Info
	{
		public static string[] Categories = new string[] { };
		public static string BaseUrl = "http://www.randommash.com/Standard_0_1.php";

		public static async Task<HttpWebRequest> createRequest (string json)
		{
			(new CancellationTokenSource ()).Dispose ();
			var request = WebRequest.Create (BaseUrl) as HttpWebRequest; 
			request.Method = "POST";	
			request.ContentType = "application/x-www-form-urlencoded";

			byte[] postData = Encoding.UTF8.GetBytes (json);
			using (var stream = await Task.Factory.FromAsync<Stream> (request.BeginGetRequestStream, request.EndGetRequestStream, request)) {
				await stream.WriteAsync (postData, 0, postData.Length);
				await stream.FlushAsync ();
			}

			return request;
		}

		public static async Task<bool> GetCategories ()
		{
			string data = string.Format ("METHOD=categoriesNew");
			string json = "";

			HttpWebRequest r = await createRequest (data);

			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			JArray jsonDict = JArray.Parse (json);
			Categories = jsonDict.Values<string> ().ToArray ();
			return true;
		}
	}
}

