﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Linq;

namespace Core
{
	public class User
	{
		public User ()
		{
		}

		public async Task<JObject> Login (string Username, string Password)
		{
			string data = string.Format ("METHOD=Login&Username={0}&Password={1}", Username, Password);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			JObject jsonDict = JObject.Parse (json);
			return jsonDict;
		}

		public async Task<JObject> Register (string Username, string Password)
		{
			string data = string.Format ("METHOD=Register&Username={0}&Password={1}", Username, Password);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			JObject jsonDict = JObject.Parse (json);
			return jsonDict;
		}

		public async void Follow (string Follower, string Following)
		{
			string data = string.Format ("METHOD=FollowUnfollow&Follower={0}&Following={1}", Follower, Following);

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				return;
			}
		}

		public async Task<bool> IsFollowing (string Follower, string Following)
		{
			string data = string.Format ("METHOD=IsFollowing&Follower={0}&Following={1}", Follower, Following);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			return JObject.Parse (json) ["Following"].ToObject<bool> ();
		}

		public async Task<JObject[]> Search (string Username, string UserID)
		{
			string data = string.Format ("METHOD=Search&alias={0}&UID={1}", Username, UserID);
			string json = "";

			try {
				HttpWebRequest r = await Info.createRequest (data);
				using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
					Stream respStr = resp.GetResponseStream ();
					StreamReader reader = new StreamReader (respStr);
					json = reader.ReadToEnd ();
				}

				if (json.Length > 0) {
					JArray jsonDict = JArray.Parse (json);
					JObject[] obj = jsonDict.Values < JObject> ().ToArray ();

					return obj;
				} else
					return new JObject[] { };
			} catch (Exception ex) {
				Debug.WriteLine (ex.Message);

				return new JObject[] { };
			}
		}

		public async void RegisterNotifications (string System, string DeviceToken, bool IsBeta = false, string UID = null)
		{
			string method = (System == "iOS") ? "AddiOSDevice" : "";
			string data = string.Format ("METHOD={0}&UUID={1}&Debug={2}{3}", method, DeviceToken, IsBeta, ((UID != null) ? "&UID=" + UID : ""));

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				return;
			}
		}

		public async Task<JObject[]> GetNotifications (string UID)
		{
			string data = string.Format ("METHOD=getNotifications&UID={0}", UID);
			string json = "";

			try {
				HttpWebRequest r = await Info.createRequest (data);
				using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
					Stream respStr = resp.GetResponseStream ();
					StreamReader reader = new StreamReader (respStr);
					json = reader.ReadToEnd ();
				}

				if (json.Length > 0) {
					JArray jsonDict = JArray.Parse (json);
					JObject[] obj = jsonDict.Values < JObject> ().ToArray ();

					return obj;
				} else
					return new JObject[] { };
			} catch (Exception ex) {
				Debug.WriteLine (ex.Message);

				return new JObject[] { };
			}
		}

		public async void ReadNotification (string NotificationID, string UID)
		{
			string data = string.Format ("METHOD={0}&UID={1}&MsgID={2}", "readNotifications", UID, NotificationID);

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				return;
			}
		}
	}
}
