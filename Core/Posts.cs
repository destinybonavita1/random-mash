﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;

namespace Core
{
	public class Posts
	{
		public Posts ()
		{
		}

		public async void Post (string UID, string Category, string Post, string imageBase = null, Action completion = null)
		{
			string data = "";
			if (imageBase != null) {
				data = string.Format ("METHOD=post&UID={0}&Post={1}&Category={2}&Image={3}", UID, Post, Category, imageBase);
			} else
				data = string.Format ("METHOD=post&UID={0}&Post={1}&Category={2}", UID, Post, Category);

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				completion ();
				return;
			}
		}

		public async void DeletePost (string UID, string PostId)
		{
			string data = string.Format ("METHOD=deletePost&UID={0}&PostId={1}", UID, PostId);

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				return;
			}
		}

		public async Task<List<JObject>> GetPost (string UID, string PostId)
		{
			string data = string.Format ("METHOD=getPost&PostId={0}&UID={1}", PostId.ToString (), UID);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			if (json.Length > 0) {
				JArray jsonDict = JArray.Parse (json);
				List<JObject> obj = jsonDict.Values < JObject> ().ToList ();

				return obj;
			} else
				return new List<JObject> ();
		}

		public async Task<List<JObject>> GetPosts (string UID, string type, string category)
		{
			string data = string.Format ("METHOD={2}&Category={0}&UID={1}", category, UID.ToString (), (type == "Top") ? "getTopPosts" : (type == "Recent" ? "getRecentPosts" : (type == "Following") ? "getFollowingPosts" : (type == "Worst") ? "getWorstPosts" : "null"));
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			if (json.Length > 0) {
				JArray jsonDict = JArray.Parse (json);
				List<JObject> obj = jsonDict.Values < JObject> ().ToList ();

				return obj;
			} else
				return new List<JObject> ();
		}

		public async Task<List<JObject>> GetUserPosts (string UID, string userId, string category)
		{
			string data = string.Format ("METHOD=getMyPosts&Category={0}&UID={1}&UserId={2}", category, UID.ToString (), userId);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			if (json.Length > 0) {
				JArray jsonDict = JArray.Parse (json);
				List<JObject> obj = jsonDict.Values < JObject> ().ToList ();

				return obj;
			} else
				return new List<JObject> ();
		}

		public async Task<List<JObject>> GetPostComments (string postId)
		{
			string data = string.Format ("METHOD=getPostComments&PostId={0}", postId);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			if (json.Length > 0) {
				JArray jsonDict = JArray.Parse (json);
				List<JObject> obj = jsonDict.Values < JObject> ().ToList ();

				return obj;
			} else
				return new List<JObject> ();
		}

		public async void DeleteComment (string UID, string PostId, string CommentID, Action complete = null)
		{
			string data = string.Format ("METHOD=deleteComment&UID={0}&PostId={1}&CommentID={2}", UID, PostId, CommentID);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				if (complete != null)
					complete ();

				return;
			}
		}

		public async void Comment (string UID, string PostId, string Comment, Action complete = null)
		{
			string data = string.Format ("METHOD=postComment&UID={0}&PostId={1}&Post={2}", UID, PostId, Comment);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				if (complete != null)
					complete ();

				return;
			}
		}

		public async void LikePost (string UID, string PostId)
		{
			string data = string.Format ("METHOD=likePost&UID={0}&PostId={1}", UID, PostId);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				return;
			}
		}

		public async Task<JObject[]> GetLikes (string UID, string PostId)
		{
			string data = string.Format ("METHOD=getLikes&PostId={0}&UID=", PostId, UID);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			if (json.Length > 0) {
				JArray jsonDict = JArray.Parse (json);
				JObject[] obj = jsonDict.Values < JObject> ().ToArray ();

				return obj;
			} else
				return new JObject[] { };
		}

		public async Task<JObject[]> GetDislikes (string UID, string PostId)
		{
			string data = string.Format ("METHOD=getDislikes&PostId={0}&UID=", PostId, UID);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				Stream respStr = resp.GetResponseStream ();
				StreamReader reader = new StreamReader (respStr);
				json = reader.ReadToEnd ();
			}

			if (json.Length > 0) {
				JArray jsonDict = JArray.Parse (json);
				JObject[] obj = jsonDict.Values < JObject> ().ToArray ();

				return obj;
			} else
				return new JObject[] { };
		}

		public async void DislikePost (string UID, string PostId)
		{
			string data = string.Format ("METHOD=dislikePost&UID={0}&PostId={1}", UID, PostId);
			string json = "";

			HttpWebRequest r = await Info.createRequest (data);
			using (var resp = await Task.Factory.FromAsync<WebResponse> (r.BeginGetResponse, r.EndGetResponse, null)) {
				return;
			}
		}
	}
}

